import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom';
import injectSheet from 'react-jss';
import { Scrollama, Step } from 'react-scrollama';
import Iframe from 'react-iframe'


const styles = {
  main: {
    padding: '10vh 2vw',
    paddingTop: '20px',
    display: 'flex',
    fontFamily: 'Helvetica',
    justifyContent: 'space-between',
  },
  graphic: {
    flexBasis: '40%',
    position: 'sticky',
    width: '100%',
    padding: '2rem 0',
    top: '160px',
    alignSelf: 'flex-start',
    backgroundColor: '#aaa',
    '& p': {
      fontSize: '5rem',
      textAlign: 'center',
      color: '#fff',
    },
  },
  scroller: {
    flexBasis: '55%',
  },
  step: {
    margin: '0 auto 2rem auto',
    paddingTop: 20,
    paddingBottom: 20,
    '& p': {
      textAlign: 'left',
      padding: '1rem',
      fontSize: '1.5rem',
    },
    '&:last-child': {
      marginBottom: 0,
    },
  },
};

class Heading extends PureComponent {
  render() {
    return (
      <div
        style={{
          backgroundColor: "#3434eb",
          color: "white"
        }}
      >
        <div
          style={{
            padding: "42vh",
            fontFamily: "Raleway",
            textAlign: "center",
          }}
        >
          <h1
            style={{
              margin: "0",
            }}
          >
            A future with quantum computers: explored
            </h1>
          <h4>By: Zach Cristol</h4>
          <h5
            style={{
              color: "#b6c4d9",
            }}

          >November 23, 2019</h5>
        </div>
      </div>
    )
  }
}

class Lead extends PureComponent {
  render() {
    return (
      <div
        style={{
          width: "43%",
          margin: "50px",
          fontFamily: "Raleway",
          fontSize: "2rem"
        }}
      >
        <p>
          Quantum computers have the potential of speeding up certain processes. This means that their use case will be significant, but defined to a specific domain of problems. The impact they have on these domains will have serious consequences.
        </p>
      </div>
    )
  }
}

// class Video extends React.Component {
//   render() {
//     return (
//       <div
//         className="video"
//         style={{
//           position: "relative",
//           paddingBottom: "56.25%" /* 16:9 */,
//           paddingTop: 25,
//           height: 0
//         }}
//       >
//         <iframe
//           title="pickles"
//           style={{
//             position: "absolute",
//             top: 0,
//             left: 0,
//             width: "100%",
//             height: "100%"
//           }}
//           src={`https://www.youtube.com/watch?v=7cUeMyrdD8M`}
//           frameBorder="0"
//         />
//       </div>
//     )
//   }
// }

class Main extends PureComponent {
  render() {
    return (
      <div
        style={{
          fontFamily: "Raleway",
          fontSize: "1.3rem",
          backgroundColor: "#e6e6e6",
          padding: "40px 17%",
        }}
      >
        <div>
          <p>
            It is not necessary to understand how quantum computers work on a fundamental level in order to understand the impact they can make. The general public is aware of the impact of classical computers, despite having a detailed knowledge of a computers complex implementation. Classical computers are the computers all around us today, and have made profound impacts and improvements overtime.
        </p>
        </div>
        <div>
          <h2>
            Classical Computers Impacts and Limitations
        </h2>
          <p>
            Computers have benfited society across many industries. Every business and research sector has seen some degree of benefits since the advent of computers. The claim that quantum computers will have an equal or greater impact is often not quantified or contextualized correctly.
        </p>

          <Iframe url="https://storage.googleapis.com/cristolzach8347/archiveFootage.mp4"
            width="100%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />
          <p
            style={{
              backgroundColor: "#e6e6e6",
              padding: "0px 0px 10px 40px",
              color: "#4f4f4f",
              fontStyle: "italic",
              fontSize: "14px"
            }}
          >
            IBM archival footage showing the need and imapct of computers in the modern world.
        </p>
          <p>
            "Overtime, computers utility and impact has grown. There are two ways that a computer can become better at solving a problem: a faster computer, or a faster algorithim." says Dr. Stewart, a retired IBM systems engineer.
          </p>
          <p>
            Moore’s law is an observation on classical computing overtime. The claim is that there is a doubling of transistors, and halving of price for a computer year over year. This translates to computers becoming exponentially faster and exponentially cheaper overtime. Computers not only become a better tool to solve problems, but also become more available for people to use them in new, amazing ways.
          </p>

          <Iframe url="https://storage.googleapis.com/cristolzach8347/mooresLaww-lowres.mp4"
            width="100%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />

          <p
            style={{
              backgroundColor: "#e6e6e6",
              padding: "0px 0px 10px 40px",
              color: "#4f4f4f",
              fontStyle: "italic",
              fontSize: "14px"
            }}
          >
            Visualization of Moore’s Law predicted number of transitor growth overtime compared to the actual transitor number growth. This illustrates the observed exponential computer speed growth overtime.
        </p>
          <p>
            The second way that a computer can become better at solving problems is by creating better algorithims. Computation theory catagorizes problems that can be solved by a computer, and attempts to rank the efficiency of algorithims. For instance, given two algorithms that solve the same problem on the same computer, one algorithm might find the answer quicker.
        </p>
          <Iframe url="https://www.desmos.com/calculator/x5nn5zgjse?embed"
            width="100%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />
          <p
            style={{
              backgroundColor: "#e6e6e6",
              padding: "0px 0px 10px 40px",
              color: "#4f4f4f",
              fontStyle: "italic",
              fontSize: "14px"
            }}
          >
            Here is a graphical representation of how algorithims are catagorized. The faster algorithim here is said to have a better "computational complexity", and can process the same amount of data in a shorter amount of time.
        </p>

          <p>
            The problems that computers solve have underlying algorithms used to solve the given problem. In order to get speedups in solving these problems, either the computer can get faster, or we can discover a clever way to use a new, faster algorithm.
        </p>
          <p>
            With quantum computers on the horizon, it is important to note that quantum computers will not help us by being a faster alternative to classical computers. Quantum computers will provide speedups to certain algorithms used to solve some problems.
        </p>
          <p>
            "Quantum complexity has been researched for quite awhile, so we are pretty aware of what problems they will benefit in. But even after we have a quantum computer, it will take time for them to become good enough to deliver on those promises." Says Emran Shafaq, former quantum research group member at the University of Texas at Austin and current software developer at Microsoft.
        </p>
        </div>
      </div>
    )
  }
}



class SecondMain extends PureComponent {
  render() {
    return (
      <div
        style={{
          fontFamily: "Raleway",
          fontSize: "1.3rem",
          backgroundColor: "#e6e6e6",
          padding: "40px 17%",
        }}
      >
        <div>
          <h2>
            Meaningful Impacts of Quantum Computers
        </h2>
          <p>
            The speedups that quantum computers will make to search will be welcomed, but will probably not have a profound impact overtime. "So the current search takes O(log(N)) time whereas a quantum search using Grovers algorithim would be O(sqrt(N)). Many problems in the real world use search, but because this speed up isn't anything crazy, this optimization wouldn't make a huge impact." says Will Sherwood, Computer Science Applications Researcher at the Texas Advanced Computing Center.
        </p>
          <p>
            With regards to encryption, there already exist encryption standards that could not be broken by classical or quantum computers. Because of this, some might assume that this quantum speedup is not very significant. "Something that I haven't seen discussed much is the security vulnurability of using an encryption standard that will be broken by quantumm computers. If people store encrypted messages today, all of those messages could be read once quantum computing technology improves." The damages that this implies spans individual privacy and national security. Governments and individuals could be saving encrypted messages today in hopes of reading these messages in the future. As internet usage continues to increase across the world, this problem will become more concerning overtime.
          </p>
          <Iframe url="https://ourworldindata.org/grapher/share-of-individuals-using-the-internet"
            width="100%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />

          <p
            style={{
              backgroundColor: "#e6e6e6",
              padding: "0px 0px 10px 40px",
              color: "#4f4f4f",
              fontStyle: "italic",
              fontSize: "14px"
            }}
          >
           Max Roser, Hannah Ritchie and Esteban Ortiz-Ospina (2019) - "Internet". Published online at OurWorldInData.org. Retrieved from: 'https://ourworldindata.org/internet' [Online Resource] 
        </p>
          <p>
            One major impact that can exploit quantum physics simulations are the research benefits of pharmacology. "Currently, it is too hard to simulate the interactions of drugs on our bodies percisly, so we typically opt for pharmalogical studies. If we had a system to accuratly simulate the effects of different chemicals in isolation, I could see how there could be significant progress made." says Jackson Lightfoot, a Neroscience Researcher at the University of Texas at Austin.
          </p>
          <p>
            Quantum computers may give us the ability to simulate real quantum effects for any practical application that relies on physics simulations. For this use case, the practicality will be determined on a case by case basis.
          </p>
          <p>
            The impact that quantum computers can make has extreme promise. However, engineering efforts take time, and current quantum are far too primitive to solve any of these listed domains. We are at a time now where we can and must prepare for a future with quantum computers. The benefits they can provide might be maliciously allocated to a small group of people, and the security concerns they pose may impact all internet users. The conversation needs to start today regarding the quantum computers of tomorrow.
          </p>


        </div>
      </div>
    )
  }
}

class Extension extends PureComponent {
  render() {
    return (
      <div
        style={{
          fontFamily: "Raleway",
          fontSize: "1.3rem",
          // backgroundColor: "#e6e6e6",
          padding: "40px 17%",
        }}
      >
        <h2>
          Quantum Computer Benefits
        </h2>
        <p>
          Generally, quantum computers are better at three things when compared to classical computers: search, factoring, and studying quantum physics. Many practical applications exploit these speedups to solve a range of subproblems. Let's understand the domain of these problems in a little more detail.
        </p>
      </div>
    )
  }
}




class Graphic extends PureComponent {
  state = {
    data: "Search",
    steps: [
      ["The search problem is just what it sounds like, and is used everywhere in the digital world. A generic example would be if you had a list of things, and you need to find a given element in the list. In the real world, a database could store a list of users. Quantum computers can run a particular algorithim that produces a search speed up. This speed up is great, but not as impactful as other speedups that quantum computers have to offer.", "Search"],
      ["The inability for classical computers to efficiently factor large numbers is an integral part of the current encryption standards. All messages sent over the air can be encrypted such that only the sender and reciever know the contents of the message. Quantum computers will have the ability to quickly factor large numbers. This means that quantum computers could break and read any encrypted message rather quickly. \"Many publications have an alarmist view when it comes to quantum computing and encryption. It is the case that quantum computers will break the current standard of encryption, but we can move to a different encryption standard that quantum computers and classical computers can't break.\", says Sam Gunn, quantum complexity researcher at the University of Texas at Austin.", "Factoring"],
      ["\"I think physics simulations are going to be where most breakthroughs in quantum computers happen. Basically, because we will have a machine that can correctly simulate quantum effects, we can better model physical systems. Our current simulations of physical systems typically lose percision because they do not simulate quantum effects, or take too long to be practical. In many cases, this loss of percision does not matter, but there are many systems that will benefit from this percision.\" says Gunn. ", "Quantum Physics"],
    ],
    graphs:[
          <Iframe url="https://www.desmos.com/calculator/eey9ixwgup?embed"
            width="99%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />,
          <Iframe url="https://www.desmos.com/calculator/72ubvxis9g?embed"
            width="99%"
            height="450px"
            id="myId"
            className="myClassname"
            display="initial"
            position="relative" />,
    ],
  };

  onStepEnter = ({ element, data }) => {
    console.log("step enter");
    element.style.backgroundColor = "#c0ced1";
    this.setState({ data });
  };

  onStepExit = ({ element }) => {
    console.log("step exit");
    element.style.backgroundColor = '#fff';
  };

  render() {
    const { data, steps, graphs } = this.state;
    const { classes } = this.props;

    return (
      <div className={classes.main}>
        <div className={classes.scroller}>
          <Scrollama onStepEnter={this.onStepEnter} onStepExit={this.onStepExit} offset={0.33}>
            {steps.map((value, index) => (
              <Step data={value[1]} key={value[0]}>
                <div className={classes.step}>
                  <p>{value[0]}</p>
                  {graphs[index]}
                </div>
              </Step>
            ))}
          </Scrollama>
        </div>
        <div className={classes.graphic}>
          <p>{data}</p>
        </div>
      </div>
    );
  }
}


const StyledGraphic = injectSheet(styles)(Graphic);

ReactDOM.render(<React.Fragment>
  <Heading />
  <Lead />
  <Main />
  <Extension />
  <StyledGraphic />
  <SecondMain />
</React.Fragment>, document.getElementById('root'));

